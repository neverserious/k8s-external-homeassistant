[![pipeline status](https://gitlab.com/neverserious/k8s-external-homeassistant/badges/main/pipeline.svg)](https://gitlab.com/neverserious/k8s-external-homeassistant/-/commits/main) 
# k8s-external-homeassistant

## A useful configuration to put Kubernetes Ingress in front of an external HomeAssistant instance.

Disclaimer: This code is my own, for my own purpose. There is no implied warranty for its use, and you are on your own to support it if you do.
