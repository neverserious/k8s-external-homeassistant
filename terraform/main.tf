resource "kubernetes_namespace" "homeassistant" {
  metadata {
    name = var.application_namespace
  }
}

resource "kubernetes_service" "homeassistant" {
  metadata {
    name      = var.application_name
    namespace = var.application_namespace
  }
  spec {
    type          = "ExternalName"
    external_name = var.external_url
  }
}

resource "kubernetes_ingress_v1" "homeassistant-http" {
  metadata {
    name      = "${var.application_name}-http"
    namespace = var.application_namespace
    annotations = {
      "cert-manager.io/cluster-issuer"                 = var.site_visibility
      "nginx.ingress.kubernetes.io/force-ssl-redirect" = "true"
    }
  }
  spec {
    rule {
      host = "${var.application_name}.${var.domain_name}"
      http {
        path {
          path = "/"
          backend {
            service {
              name = var.application_name
              port {
                number = var.external_port
              }
            }
          }
        }
      }
    }
    tls {
      hosts = [
        "${var.application_name}.${var.domain_name}"
      ]
      secret_name = "${var.application_name}-http-tls"
    }
  }
}

module "dns" {
  source                = "git::https://gitlab.com/neverserious/terraform-module-dns.git"
  freeipa_host          = var.freeipa_host
  freeipa_username      = var.freeipa_username
  freeipa_password      = var.freeipa_password
  freeipa_insecure      = true
  cloudflare_token      = var.cloudflare_token
  cloudflare_proxied    = false
  domain_internalname   = var.domain_name
  record_internalname   = var.application_name
  record_internaltarget = ["${var.domain_name}."]
  domain_externalname   = var.domain_name
  record_externalname   = var.application_name
  record_externaltarget = "${var.domain_name}."
}
