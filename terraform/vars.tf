# Leave the rest of these alone. 
variable "freeipa_host" { sensitive = true }
variable "freeipa_username" { sensitive = true }
variable "freeipa_password" { sensitive = true }

variable "kube_configpath" { sensitive = true }
variable "domain_name" { sensitive = true }
variable "application_name" { default = "bitwarden" }
variable "application_namespace" { default = "bitwarden" }
variable "deploy_env" {}
variable "site_visibility" { default = "internal" }
variable "cloudflare_token" { sensitive = true }
variable "external_url" { sensitive = true }
variable "external_port" { sensitive = true }
